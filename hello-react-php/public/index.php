<?php
declare(strict_types=1);

use Psr\Http\Message\ServerRequestInterface;
use React\Http\Response;
use React\Http\Server;

require_once __DIR__ . '/../vendor/autoload.php';

$loop = React\EventLoop\Factory::create();

$server = new Server(static function (ServerRequestInterface $request) {
    return new Response(
      200,
      ['Content-Type' => 'text/plain'],
      "Hello World!\n"
    );
});
$port = 8000;

echo "Starting up on port $port\n";

$socket = new React\Socket\Server("0.0.0.0:$port", $loop);
$server->listen($socket);

$loop->run();
